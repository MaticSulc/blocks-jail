package com.zenya.blocksjail.events;

import com.zenya.blocksjail.storage.DataCache;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;

public class JailBlockReduceEvent extends Event implements Cancellable {
    private BlockBreakEvent event;
    private static DataCache dataCache = DataCache.getInstance();

    public JailBlockReduceEvent(BlockBreakEvent event) {
        this.event = event;
        this.triggerEvent();
    }

    public void triggerEvent() {
        Player player = event.getPlayer();
        Integer blocksLeft = dataCache.getBlocksLeft(player);

        if(blocksLeft != null && blocksLeft > 0) {
            dataCache.registerPlayer(player, blocksLeft-1);
        }
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void setCancelled(boolean b) {

    }

    @Override
    public HandlerList getHandlers() {
        return null;
    }
}

