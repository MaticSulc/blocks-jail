package com.zenya.blocksjail.events;

import com.zenya.blocksjail.main.BlocksJail;
import com.zenya.blocksjail.storage.DataCache;
import com.zenya.blocksjail.utilities.ChatUtils;
import com.zenya.blocksjail.utilities.ConfigManager;
import com.zenya.blocksjail.utilities.DBManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Listeners implements Listener {
    private static ConfigManager configManager = ConfigManager.getInstance();
    private static DBManager dbManager = DBManager.getInstance();
    private static DataCache dataCache = DataCache.getInstance();

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // Register in DB
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    dbManager.initPlayerData(player);
                } catch(Exception e) {
                    // Do nothing
                }
            }
        }.runTaskAsynchronously(BlocksJail.getInstance());

        // Register in cache
        new BukkitRunnable() {
            @Override
            public void run() {
                dataCache.registerPlayer(player, dbManager.getBlocksLeft(player));
            }
        }.runTaskLaterAsynchronously(BlocksJail.getInstance(), 1L);
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        // Update in DB
        new BukkitRunnable() {
            @Override
            public void run() {
                dbManager.setBlocksLeft(player, dataCache.getBlocksLeft(player));
            }
        }.runTaskAsynchronously(BlocksJail.getInstance());

        // Unregister in cache
        new BukkitRunnable() {
            @Override
            public void run() {
                dataCache.unregisterPlayer(player);
            }
        }.runTaskLaterAsynchronously(BlocksJail.getInstance(), 1L);
    }

    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Integer blocksLeft = dataCache.getBlocksLeft(player);
        Integer blocksLeftMsgFix = dataCache.getBlocksLeft(player)-1; 
        String jailWorld = configManager.getJailWorld().getName();
        
        for(Player p : Bukkit.getServer().getOnlinePlayers()) {
            if(p.hasPermission("blocksjail.bypass")) {
                continue;
            }

            // Send packets to jailed players
            if(p.getWorld().getName().equals(jailWorld) && blocksLeft != null && blocksLeft > 0) {
                String message = "&c" + blocksLeftMsgFix + " blocks remaining";
                ChatUtils.sendSubtitle(p, message);
            }

        if(event.isCancelled()) {
            return;
        }

        if(player.hasPermission("blocksjail.bypass")) {
            return;
        }

        if(blocksLeft == null || blocksLeft <= 0) {
            return;
        }

        if(player.getWorld().getName().equals(configManager.getJailWorld().getName()) && !configManager.getCanDropBlocks()) {
        	event.setCancelled(true);
        	event.getBlock().getLocation().getBlock().setType(Material.AIR); 
            event.setExpToDrop(0);
        }

        if(configManager.getBlacklistedMaterials() != null && configManager.getBlacklistedMaterials().size() != 0 && configManager.getBlacklistedMaterials().contains(event.getBlock().getType())) {
            return;
        }

        try {
            Bukkit.getPluginManager().callEvent(new JailBlockReduceEvent(event));
        } catch(Exception e) {
            // Silence errors

        }
        }

    }

    @EventHandler
    public void onPlayerCommandPreProcessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        Integer blocksLeft = dataCache.getBlocksLeft(player);

        if(player.hasPermission("blocksjail.bypass")) {
            return;
        }

        if(blocksLeft == null || blocksLeft <= 0) {
            return;
        }

        if(configManager.getBlacklistedCommands() == null || configManager.getBlacklistedCommands().size() == 0) {
            return;
        }

        String command = event.getMessage().replace("/", "");
        for(String cmd : configManager.getBlacklistedCommands()) {
            if(command.startsWith(cmd) || cmd.equals("*")) {
                ChatUtils.sendMessage(player, "&cYou cannot use this command when jailed");
                event.setCancelled(true);
            }
        }
    }

}
